import Head from 'next/head';
import Navbar from './Components/Navbar.js';
import LowerPanel from './Components/LowerPanel.js';
import Editor from "./Components/Editor.js";
import Terminal from './Components/Terminal.js';
import CompilingDialog from './Components/CompilingDialog.js';
import LessonSelector from './Components/LessonSelector.js';

import { withRouter } from 'next/router';

class IDE extends React.Component {
  componentDidMount() {
    console.log("finish load");
    // console.log();
  }
  render(){
    return (
      <div className="viewport">
        <Head>
          <title>LEARN | IDE</title>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous" />
          <link rel="stylesheet" href="/ide.css" />
          <link rel="stylesheet" href="xterm.css" />
          <script src="/ace-build/build/src/ace.js" type="text/javascript" charset="utf-8"></script>
        </ Head>
        <Navbar />
        <div class="view-wrapper">
          <LessonSelector />
          <Editor />
          <Terminal />
        </div>
        <LowerPanel title={this.props.router.query.class}/>
        <CompilingDialog />
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      </div>
    )
  }
};

export default withRouter(IDE);
