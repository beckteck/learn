import axios from 'axios';

export function compile(){
  console.log(getEditor());
  $("#compileDialog").modal('show');
  axios.post('/compile', {
    code: getEditor()
  }).then((data) => {
    console.log(data.data);
    updateTerminal(`/store/${data.data}.html`);
    $('#compileDialog').modal('hide');
  })
}
