export default class Terminal extends React.Component {
  constructor(props){
    super(props);
    this.state = {url: "/blank_term.html"};
  }
  componentDidMount() {
    window.updateTerminal = (url) => {
      this.setState({url: url});
    }
  }
  render() {
    return (<iframe id="terminal" src={this.state.url} width="300px" height="500px" scrolling="no"></iframe>);
  }
}
