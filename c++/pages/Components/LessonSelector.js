export default class LessonSelector extends React.Component{
  constructor(props) {
    super(props);
    this.state = {sidebarStyle: {}, state: "closed"};
    this.openSelector = this.openSelector.bind(this);
  }
  openSelector() {
    this.setState({
      img: "/img/hand_open.png",
      sidebarStyle: {
        visibility: "visible",
        margin: 0
      },
      handStyle: {
        "margin-left": 0
      },
      state: "open"
    });
    /* setTimeout(() => { */
      this.setState({
        stuffStyle: {
          "top" : "56px",
          "margin-top": 0
        }
      });
    /* }, 820); */
  }
  closeSelector() {
    this.setState({
      sidebarStyle: {
        "margin-right": "-500px",
        "visibility" : "visible"
      },
      stuffStyle: {
        "top" : "56px",
        "margin-top": 0
      },
      state: "closed"
    })
  }
  toggleSelector() {
    if(this.state.state == "closed"){
      this.openSelector();
    }else if(this.state.state == "open") {
      this.closeSelector();
    }
  }
  componentDidMount() {
    var parent = this;
    window.LessonSelectorOpen = function() {
      parent.openSelector();
    }
    window.LessonSelectorClose = () => {
      parent.closeSelector();
    }
    window.LessonSelectorToggle = () => {
      parent.toggleSelector();
    }
  }
  render() {
    return(
      <div className="sidebar" style={this.state.sidebarStyle}>
        /* <img src={this.state.img} className="magic-hand" style={this.state.handStyle}/> */
        <div className="actualStuff" style={this.state.stuffStyle}>
          <p>Test</p>
        </div>
      </div>
    );
  }
}
