export default class Editor extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    var editor = ace.edit('editor');
    editor.setTheme("ace/theme/monokai");
    window.getEditor = () => {
      return(editor.getValue());
    }
  }
  render() {
    return (
      <div id="editor">Content</div>
    );
  }
}
