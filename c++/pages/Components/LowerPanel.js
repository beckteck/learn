import  "../Script.js";

import * as ideUtilty from '../Script.js'

export default class LowerPanel extends React.Component{
  constructor(props){
    super(props);
  }
  sidebar() {
    window.LessonSelectorToggle();
  }
  render() {
    return(
      <div className="lowerPanel">
        <span className="nav-center-text" onClick={this.sidebar}>{this.props.title}</span>
        <div className="panelRight">
          <button type="button" className="nav-btn-full compile" onClick={ideUtilty.compile}>Compile</button>
        </div>
      </div>
    );
  }
}
