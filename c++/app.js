var spawn = require('child_process').spawn;
var crypto = require('crypto');
var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var nano = require('nano')("http://admin:1234@127.0.0.1:5984");
var bcrypt = require('bcryptjs');
var session = require('express-session');
var next = require('next');
var nApp = next({
  dev: true
});

nApp.prepare().then(() => {
  var app = express();
  var http = require('http').Server(app);
  var io = require('socket.io')(http);

  app.use(session({
    secret: 'flsdkl;fjsadlkfjeifjdfsdfueuihfdjklhfASDFGASDFASDFsdfasdfasdFASDRWERHGHGFhsdfgdfgTYTYUTRYGHhghgfhFFGG',
    cookie: {
      maxAge: 5000
    }
  }));

  var requirements = [
    {
        type: "match-string",
        text: "#include<iostream>"
    },
    {
      type: "line-requirements",
      lines: [
        "int",
        "=",
        ";"
      ]
    }
  ];

  function validateCode(requirements, code){
    var checks = [

    ];
    code = code.split(/\r?\n/);
    code.forEach((line) => {
      requirements.forEach((check) => {
        if(check.type == "match-string"){
          if(line.indexOf(check.text) > -1){
            checks.push(check);
          }
        }else if(check.type == "line-requirements"){
          var tmpLines = [

          ];
          check.lines.forEach((seg) => {
            if(line.indexOf(seg) > -1){
              tmpLines.push(seg);
            }
          });
          if(tmpLines.toString() == check.lines.toString()){
            checks.push(check);
          }
        }
      });
    });
    return checks;
  }

  // var code = `#include<iostream> \n int main(){ \n std::cout << "test" << std::endl; \n }`;

  function compile(code, callback){
    var fileName = crypto.randomBytes(128).toString('hex').slice(0, 5).toUpperCase();

    fs.writeFile(`./tmp/${fileName}.cpp`, code, (err) => {
      console.log(`[FS] ./tmp/${fileName}.cpp file created`);
      var run = spawn("emcc", [`./tmp/${fileName}.cpp`, '-o', `./tmp/${fileName}.html`, '--shell-file', 'shell.html']);
      run.stdout.on('data', (data) => {
        console.log(data.toString());
      });
      run.stderr.on('data', (data) => {
        console.log(data.toString());
      });
      run.on('exit', () => {
        callback(fileName);
      });
    });
  }

  app.use(express.static('public'))
  app.get('/', (req, res) => {
    res.sendFile(__dirname + "/landing.html");
  });

  app.use(bodyParser());
  app.post('/compile', (req, res) => {
    console.log(req.body);
    compile(req.body.code, (fileName) => {
      res.send(fileName);
      setTimeout(() => {
        var ext = [
          '.cpp',
          '.js',
          '.html',
          '.wasm'
        ];
        ext.forEach((ext) => {
          fs.unlink(`./tmp/${fileName}${ext}`, (err) => {
            console.log(err);
            console.log(`[FS] Deleted file ./tmp/${fileName}${ext}`);
          });
        });
      }, 100000);
    });
  });

  app.use('/store', express.static('tmp'));

  // io.on('connection', (socket) => {
  //
  //
  //   socket.on("cmd", (data) => {
  //
  //   });
  // });

  const account = nano.use("account");

  app.get('/profile/new', (req, res) => {
    res.sendFile(__dirname + "/new_user.html");
  });

  app.use(bodyParser());
  app.post('/profile/new', (req, res) => {
    if(req.body.email != undefined && req.body.secret != undefined) {
      account.head(req.body.email, (err, body, header) => {
        if(err != undefined && header == undefined){
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(req.body.secret, salt, (err, hash) => {
              account.insert({_id: req.body.email, secret: hash}).then((data) => {
                res.send({
                  status: "ok"
                });
              })
            });
          });
        } else {
          res.send({
            status: "bad",
            error: "Account exist"
          });
        }
      })
    } else {
      res.send({
        status: "bad",
        error: "Not good data"
      });
    }
  });

  app.get('/profile/login', (req,res) => {
    if(req.session.email == undefined){
      res.sendFile(__dirname + "/login.html");
    }else {
      res.send("LOL");
    }
  });

  app.post('/profile/login', (req, res) => {
    if(req.body.email != undefined && req.body.secret != undefined) {
      account.get(req.body.email).then((db_info) => {
        bcrypt.compare(req.body.secret, db_info.secret, (err, secretCheck) => {
          req.session.email = req.body.email;
          res.send("K");
        });
      })
    }
  });

  app.get('/profile/logout', (req, res) => {
    if(req.session.email == undefined){
      res.send({
        status: "error",
        error: "Not even logged in"
      });
    }else {
      req.session.destroy((err) => {
        res.redirect('/profile/login');
      })
    }
  });

  app.get("/class/:class/", (req, res) => {
    nApp.render(req, res, "/ide", {
      class: req.params.class
    });
  });

  app.use("/_next", express.static(".next"))

  http.listen(8000, () => {
    console.log('[Web] Started');
  });
})
